package com.puravida.groogle

import com.google.api.client.util.DateTime
import com.google.api.services.calendar.Calendar
import com.google.api.services.calendar.model.Event
import com.google.api.services.calendar.model.EventAttendee
import com.google.api.services.calendar.model.EventDateTime
import com.google.api.services.calendar.model.EventReminder

class CreateEvent {

    protected Calendar service

    protected String calendarId

    Event event = new Event()

    private EventDateTime start, end
    void allDay( String yyMMdd){
        event.end=event.start=new EventDateTime(date: new DateTime(yyMMdd))
    }
    void from( Date start ){
        event.start=new EventDateTime(dateTime: new DateTime(start))
    }
    void until( Date start ){
        event.end=new EventDateTime(dateTime: new DateTime(start))
    }
    void between( Date start, Date end){
        event.start=new EventDateTime(dateTime: new DateTime(start))
        event.end=new EventDateTime(dateTime: new DateTime(end))
    }

    void attendee( String email ){
        attendees([email])
    }
    void attendees( String[]emails ){
        attendees(emails as List<String>)
    }
    void attendees( List<String> emails ){
        List<EventAttendee> attendees = event.attendees ?: []
        attendees.add emails.collect{ new EventAttendee(email: it)}
        event.attendees = attendees
    }

    void reminder(String method, int minutes){
        List<EventReminder> overrides = event.reminders?.overrides ?: []
        overrides.add new EventReminder(method: method, minutes: minutes)
        Event.Reminders reminders = new Event.Reminders(useDefault: false,overrides:overrides)
        event.reminders = reminders
    }

    Event execute(){
        event=service.events().insert(calendarId,event).execute()
        event
    }

}

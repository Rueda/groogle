package groogle.rest

import com.google.api.services.drive.DriveScopes
import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.GroogleScript
import com.puravida.groogle.SheetScript
import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

class SheetControllerSpec extends Specification implements ControllerUnitTest<SheetController> {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
        expect:"fix me"
        true == true
    }

    void "list sheet as service"(){
        given:
        GroogleScript.instance.login{

            applicationName 'groogle-example'

            withScopes DriveScopes.DRIVE, SheetsScopes.SPREADSHEETS

            usingCredentials '/groogle-688bcfc07d1b.json'

            asService true
        }
        SheetScript.instance.groogleScript=GroogleScript.instance

        expect:"list of sheet"
        controller.index() != null
    }

}
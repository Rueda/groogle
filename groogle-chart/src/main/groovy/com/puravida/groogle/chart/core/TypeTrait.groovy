package com.puravida.groogle.chart.core

trait TypeTrait {

    abstract String getTYPE();

    Map<String,String>getTypeAsMap(){
        ['cht':TYPE]
    }

    String getTypeAsString(){
        "cht=$TYPE"
    }
}
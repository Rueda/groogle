package com.puravida.groogle

import com.google.maps.GeoApiContext
import com.google.maps.PlacesApi
import com.google.maps.TextSearchRequest
import com.google.maps.model.PlacesSearchResult
import groovy.transform.CompileStatic

@CompileStatic
class PlacesApiSpec {

    protected GeoApiContext geoApiContext


    void execute(){
        TextSearchRequest textSearchRequest = PlacesApi.textSearchQuery(geoApiContext, 'pescara')
        textSearchRequest.await().results.each{PlacesSearchResult placesSearchResult->
            println placesSearchResult.formattedAddress
        }
    }
}

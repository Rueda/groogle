# Groogle Changelog

## Tag v1.5.0-alpha4
### Issue 14 

**Reviewed all closures with rehydrate**

 * Closes #14

[b8c897f465d3828](https://gitlab.com/puravida-software/groogle/commit/b8c897f465d3828) jorge *2018-05-14 04:51:29*


### Issue 18 

**Evaluate scripts as text or files and create defines for scopes in core**

 * Closes #19 and #18

[1bdfe7aead1bd4c](https://gitlab.com/puravida-software/groogle/commit/1bdfe7aead1bd4c) jorge *2018-05-14 04:45:13*


### Issue 19 

**Evaluate scripts as text or files and create defines for scopes in core**

 * Closes #19 and #18

[1bdfe7aead1bd4c](https://gitlab.com/puravida-software/groogle/commit/1bdfe7aead1bd4c) jorge *2018-05-14 04:45:13*


### No issue

**Prepare version 1.5.0-alpha4**


[9e7087050515b38](https://gitlab.com/puravida-software/groogle/commit/9e7087050515b38) jorge *2018-05-14 04:44:51*


## Tag v1.5.0-alpha3
### Issue 17 

**Bump versions for google apis (calendar,drive,sheets)**

 * - com.google.apis:google-api-services-calendar [v3-rev276-1.23.0 -&gt; v3-rev318-1.23.0]
 * - com.google.apis:google-api-services-drive [v3-rev94-1.23.0 -&gt; v3-rev117-1.23.0]
 * - com.google.apis:google-api-services-sheets [v4-rev496-1.23.0 -&gt; v4-rev522-1.23.0]
 * - com.jfrog.bintray.gradle:gradle-bintray-plugin [1.7.3 -&gt; 1.8.0]
 * Closes #17

[dcdf16594b8435c](https://gitlab.com/puravida-software/groogle/commit/dcdf16594b8435c) jorge *2018-05-10 18:25:54*


### Issue 7 

**Add a new plugin for changelog**

 * Close #7 but still need improvement

[fde4e9d5f0f152b](https://gitlab.com/puravida-software/groogle/commit/fde4e9d5f0f152b) jorge *2018-05-06 19:34:49*


### Issue 8 

**First dependency update report generated:**

 * The following dependencies are using the latest milestone version:
 * - com.puravida.asciidoctor:asciidoctor-extensions:1.7
 * - com.puravida.asciidoctor:asciidoctor-themes:1.3
 * - commons-httpclient:commons-httpclient:3.1
 * - gradle.plugin.se.bjurr.gitchangelog:git-changelog-gradle-plugin:1.55
 * - com.google.api-client:google-api-client:1.23.0
 * - com.google.oauth-client:google-oauth-client-jetty:1.23.0
 * - com.github.ben-manes:gradle-versions-plugin:0.17.0
 * - org.apache.httpcomponents:httpclient:4.5.5
 * - junit:junit:4.12
 * - org.spockframework:spock-core:1.1-groovy-2.4
 * - org.ysb33r.gradle:vfs-gradle-plugin:1.0
 * The following dependencies have later milestone versions:
 * - org.asciidoctor:asciidoctor-gradle-plugin [1.5.3 -&gt; 1.5.7]
 * - com.google.apis:google-api-services-calendar [v3-rev276-1.23.0 -&gt; v3-rev318-1.23.0]
 * - com.google.apis:google-api-services-drive [v3-rev94-1.23.0 -&gt; v3-rev117-1.23.0]
 * - com.google.apis:google-api-services-sheets [v4-rev496-1.23.0 -&gt; v4-rev522-1.23.0]
 * - com.jfrog.bintray.gradle:gradle-bintray-plugin [1.7.3 -&gt; 1.8.0]
 * - org.codehaus.groovy:groovy-all [2.4.10 -&gt; 3.0.0-alpha-2]
 * - org.codehaus.groovy:groovy-all [2.4.12 -&gt; 3.0.0-alpha-2]
 * Closes #8

[cd8355a004e9705](https://gitlab.com/puravida-software/groogle/commit/cd8355a004e9705) jorge *2018-05-10 18:11:09*


### Issue alpha-2 

**First dependency update report generated:**

 * The following dependencies are using the latest milestone version:
 * - com.puravida.asciidoctor:asciidoctor-extensions:1.7
 * - com.puravida.asciidoctor:asciidoctor-themes:1.3
 * - commons-httpclient:commons-httpclient:3.1
 * - gradle.plugin.se.bjurr.gitchangelog:git-changelog-gradle-plugin:1.55
 * - com.google.api-client:google-api-client:1.23.0
 * - com.google.oauth-client:google-oauth-client-jetty:1.23.0
 * - com.github.ben-manes:gradle-versions-plugin:0.17.0
 * - org.apache.httpcomponents:httpclient:4.5.5
 * - junit:junit:4.12
 * - org.spockframework:spock-core:1.1-groovy-2.4
 * - org.ysb33r.gradle:vfs-gradle-plugin:1.0
 * The following dependencies have later milestone versions:
 * - org.asciidoctor:asciidoctor-gradle-plugin [1.5.3 -&gt; 1.5.7]
 * - com.google.apis:google-api-services-calendar [v3-rev276-1.23.0 -&gt; v3-rev318-1.23.0]
 * - com.google.apis:google-api-services-drive [v3-rev94-1.23.0 -&gt; v3-rev117-1.23.0]
 * - com.google.apis:google-api-services-sheets [v4-rev496-1.23.0 -&gt; v4-rev522-1.23.0]
 * - com.jfrog.bintray.gradle:gradle-bintray-plugin [1.7.3 -&gt; 1.8.0]
 * - org.codehaus.groovy:groovy-all [2.4.10 -&gt; 3.0.0-alpha-2]
 * - org.codehaus.groovy:groovy-all [2.4.12 -&gt; 3.0.0-alpha-2]
 * Closes #8

[cd8355a004e9705](https://gitlab.com/puravida-software/groogle/commit/cd8355a004e9705) jorge *2018-05-10 18:11:09*


### Issue groovy-2 

**First dependency update report generated:**

 * The following dependencies are using the latest milestone version:
 * - com.puravida.asciidoctor:asciidoctor-extensions:1.7
 * - com.puravida.asciidoctor:asciidoctor-themes:1.3
 * - commons-httpclient:commons-httpclient:3.1
 * - gradle.plugin.se.bjurr.gitchangelog:git-changelog-gradle-plugin:1.55
 * - com.google.api-client:google-api-client:1.23.0
 * - com.google.oauth-client:google-oauth-client-jetty:1.23.0
 * - com.github.ben-manes:gradle-versions-plugin:0.17.0
 * - org.apache.httpcomponents:httpclient:4.5.5
 * - junit:junit:4.12
 * - org.spockframework:spock-core:1.1-groovy-2.4
 * - org.ysb33r.gradle:vfs-gradle-plugin:1.0
 * The following dependencies have later milestone versions:
 * - org.asciidoctor:asciidoctor-gradle-plugin [1.5.3 -&gt; 1.5.7]
 * - com.google.apis:google-api-services-calendar [v3-rev276-1.23.0 -&gt; v3-rev318-1.23.0]
 * - com.google.apis:google-api-services-drive [v3-rev94-1.23.0 -&gt; v3-rev117-1.23.0]
 * - com.google.apis:google-api-services-sheets [v4-rev496-1.23.0 -&gt; v4-rev522-1.23.0]
 * - com.jfrog.bintray.gradle:gradle-bintray-plugin [1.7.3 -&gt; 1.8.0]
 * - org.codehaus.groovy:groovy-all [2.4.10 -&gt; 3.0.0-alpha-2]
 * - org.codehaus.groovy:groovy-all [2.4.12 -&gt; 3.0.0-alpha-2]
 * Closes #8

[cd8355a004e9705](https://gitlab.com/puravida-software/groogle/commit/cd8355a004e9705) jorge *2018-05-10 18:11:09*


### No issue

**Changelog for v1.5.0-alpha3**


[e69d43b515ad26e](https://gitlab.com/puravida-software/groogle/commit/e69d43b515ad26e) jorge *2018-05-10 18:44:30*

**v1.5.0-alpha3**


[105279b123526fb](https://gitlab.com/puravida-software/groogle/commit/105279b123526fb) jorge *2018-05-10 18:27:09*

**/close**


[05468eeb694060f](https://gitlab.com/puravida-software/groogle/commit/05468eeb694060f) jorge *2018-05-10 17:54:05*

**First version of changelog**


[3ae3cd6fe360646](https://gitlab.com/puravida-software/groogle/commit/3ae3cd6fe360646) jorge *2018-05-06 19:46:05*


## Tag v1.5.0-alpha2
### No issue

**new method in Sheet appendRow(map)**


[baa5a09ba180ce3](https://gitlab.com/puravida-software/groogle/commit/baa5a09ba180ce3) jorge *2018-05-05 19:22:19*

**add copyblock a core**


[205206fdebe29eb](https://gitlab.com/puravida-software/groogle/commit/205206fdebe29eb) jorge *2018-05-05 10:09:15*

**docu improved with puravida-extensions**


[589b5a5cdeae314](https://gitlab.com/puravida-software/groogle/commit/589b5a5cdeae314) jorge *2018-05-05 08:27:16*

**docu improved**


[3549044a6f89970](https://gitlab.com/puravida-software/groogle/commit/3549044a6f89970) jorge *2018-05-03 21:14:34*

**docu drive improved**


[db28e019a460b41](https://gitlab.com/puravida-software/groogle/commit/db28e019a460b41) jorge *2018-05-03 19:21:16*

**typo at settings provoca un submodulo fantasma**


[82d90c7e659ec7c](https://gitlab.com/puravida-software/groogle/commit/82d90c7e659ec7c) jorge *2018-05-03 19:04:03*

**merge from 14-rehidrate**


[934bbdf0f72ac1b](https://gitlab.com/puravida-software/groogle/commit/934bbdf0f72ac1b) jorge *2018-05-03 18:59:58*

**Resolve "rehidratar closures"**


[d96d4da17d492d1](https://gitlab.com/puravida-software/groogle/commit/d96d4da17d492d1) Jorge Aguilera *2018-05-03 15:05:29*

**improve presentation**


[cd4da4be144a849](https://gitlab.com/puravida-software/groogle/commit/cd4da4be144a849) Jorge Aguilera *2018-05-03 14:37:14*

**change account for test**


[698811c17e167a4](https://gitlab.com/puravida-software/groogle/commit/698811c17e167a4) jorge *2018-05-03 13:50:50*

**typo at readme.adoc**


[6c2823cb8873128](https://gitlab.com/puravida-software/groogle/commit/6c2823cb8873128) jorge *2018-05-03 12:44:30*

**readed added**


[e178440c0fe64cd](https://gitlab.com/puravida-software/groogle/commit/e178440c0fe64cd) jorge *2018-05-03 12:40:27*

**Resolve "crear slides"**


[0484f2c5a2d205a](https://gitlab.com/puravida-software/groogle/commit/0484f2c5a2d205a) Jorge Aguilera *2018-05-01 09:32:29*

**Resolve "dsl-login"**


[9ee9accf6952f53](https://gitlab.com/puravida-software/groogle/commit/9ee9accf6952f53) Jorge Aguilera *2018-04-30 09:04:29*

**Resolve "dsl-login"**


[6b426a7a55ea5b4](https://gitlab.com/puravida-software/groogle/commit/6b426a7a55ea5b4) Jorge Aguilera *2018-04-30 08:56:24*

**permitir CD/CI via web**


[64c2c038ccc1ae2](https://gitlab.com/puravida-software/groogle/commit/64c2c038ccc1ae2) jorge *2018-04-22 16:14:18*

**Progress presentacion**


[acaf6643fb20316](https://gitlab.com/puravida-software/groogle/commit/acaf6643fb20316) jorge *2018-04-22 16:11:44*

**/close**


[833aad3052007e2](https://gitlab.com/puravida-software/groogle/commit/833aad3052007e2) jorge *2018-04-22 15:02:17*


## Tag 1.5.0-alpha
### No issue

**cuentas de servicio y bintray solo en proyectos necesarios**


[ee566e988e71afd](https://gitlab.com/puravida-software/groogle/commit/ee566e988e71afd) jorge *2018-04-22 14:44:03*

**Inicio v1.5.0-alpha**


[dfd7690e89ccd89](https://gitlab.com/puravida-software/groogle/commit/dfd7690e89ccd89) jorge *2018-04-22 13:51:46*

**progress slides**


[56ba5b96dd8a53b](https://gitlab.com/puravida-software/groogle/commit/56ba5b96dd8a53b) jorge *2018-04-22 13:43:21*

**slide progress**


[25405a49f06c28e](https://gitlab.com/puravida-software/groogle/commit/25405a49f06c28e) jorge *2018-04-20 17:53:47*

**WIP**


[123d008d78f3310](https://gitlab.com/puravida-software/groogle/commit/123d008d78f3310) jorge *2018-04-20 05:14:05*

**/close**


[791133c103e8382](https://gitlab.com/puravida-software/groogle/commit/791133c103e8382) jorge *2018-04-19 20:58:24*

**revisados tags para resolver version en documentacion**

 * /close

[a9a8ceea993b09b](https://gitlab.com/puravida-software/groogle/commit/a9a8ceea993b09b) jorge *2018-04-19 20:40:17*

**1.0.0.-alpha.5 en chart**


[f8f2bd4a4e28984](https://gitlab.com/puravida-software/groogle/commit/f8f2bd4a4e28984) jorge *2018-04-19 20:33:56*

**revision build.gradle para evitar duplicidades**


[f94e37ea7a62321](https://gitlab.com/puravida-software/groogle/commit/f94e37ea7a62321) jorge *2018-04-19 20:25:25*

**groogle-chart 1.0.0-alpha.4**


[b6aa4e823f102ed](https://gitlab.com/puravida-software/groogle/commit/b6aa4e823f102ed) jorge *2018-04-16 20:19:20*

**Agregar nuevo directorio**


[0261b0c5b1daf8e](https://gitlab.com/puravida-software/groogle/commit/0261b0c5b1daf8e) Jorge Aguilera *2018-04-16 20:13:47*

**add presentation**


[5fa77ba41ec1965](https://gitlab.com/puravida-software/groogle/commit/5fa77ba41ec1965) jorge *2018-04-16 19:17:05*

**add presentation**


[f869ed556b5624c](https://gitlab.com/puravida-software/groogle/commit/f869ed556b5624c) jorge *2018-04-16 19:14:16*

**organize asciidoctor dirs**


[3cb9e491ce7f0fa](https://gitlab.com/puravida-software/groogle/commit/3cb9e491ce7f0fa) jorge *2018-04-16 14:20:43*

**docu de chart no estaba generada**


[69f70236d2acb0e](https://gitlab.com/puravida-software/groogle/commit/69f70236d2acb0e) jorge *2018-03-01 09:01:28*

**Update .gitlab-ci.yml**


[15200435b1b56c1](https://gitlab.com/puravida-software/groogle/commit/15200435b1b56c1) Jorge Aguilera *2018-03-01 08:46:43*

**link docu chart with core**


[66c2c2062e5828b](https://gitlab.com/puravida-software/groogle/commit/66c2c2062e5828b) jorge *2018-03-01 08:20:34*

**link docu chart with core**


[50a69762238eceb](https://gitlab.com/puravida-software/groogle/commit/50a69762238eceb) jorge *2018-03-01 07:50:08*

**link docu chart with core**


[ccbe758325d163d](https://gitlab.com/puravida-software/groogle/commit/ccbe758325d163d) jorge *2018-03-01 07:41:38*

**groogle-char 1.0.0-alpha.2**


[8f1b1f72c38abef](https://gitlab.com/puravida-software/groogle/commit/8f1b1f72c38abef) jorge *2018-02-28 22:02:19*

**changed version methodology. Every project has is own version**


[350f30f6e060381](https://gitlab.com/puravida-software/groogle/commit/350f30f6e060381) jorge *2018-02-28 09:04:44*

**docu calendar**


[bc792e499c3e02b](https://gitlab.com/puravida-software/groogle/commit/bc792e499c3e02b) jorge *2018-02-04 16:43:22*

**primera version de calendar**


[65823e776d4018c](https://gitlab.com/puravida-software/groogle/commit/65823e776d4018c) jorge *2018-02-03 21:14:06*

**fix bug in list all files**


[d735538dbe2547a](https://gitlab.com/puravida-software/groogle/commit/d735538dbe2547a) jorge *2018-02-03 17:10:21*

**groogle-drive/src/main/groovy/com/puravida/groogle/DriveScript.groovy**


[0dc24df9248d4ef](https://gitlab.com/puravida-software/groogle/commit/0dc24df9248d4ef) jorge *2018-02-03 17:09:47*

**typo en docu**


[2d3ec23ce0ac0cf](https://gitlab.com/puravida-software/groogle/commit/2d3ec23ce0ac0cf) jorge *2018-02-01 15:38:35*

**v1.4.0 con docu revisada**


[5f441b6c2aa85b4](https://gitlab.com/puravida-software/groogle/commit/5f441b6c2aa85b4) jorge *2018-02-01 15:28:01*

**inicio v.1.4.0 sheet**


[d5b5df3e9c69c69](https://gitlab.com/puravida-software/groogle/commit/d5b5df3e9c69c69) jorge *2018-02-01 14:05:04*

**groogle-rest**


[a3fac4819b5ee48](https://gitlab.com/puravida-software/groogle/commit/a3fac4819b5ee48) jorge *2018-02-01 08:04:56*

**docu improved**


[70cb2c6fcf04d0f](https://gitlab.com/puravida-software/groogle/commit/70cb2c6fcf04d0f) jorge *2018-01-30 09:24:18*

**improve docu 1.3.1**


[7260861a095f109](https://gitlab.com/puravida-software/groogle/commit/7260861a095f109) jorge *2018-01-30 05:22:13*

**Mejora docu 1.3.1**


[01dd6148c3cfaca](https://gitlab.com/puravida-software/groogle/commit/01dd6148c3cfaca) jorge *2018-01-29 22:02:36*

**Mejora docu 1.3.1**


[662bb7c0be9883b](https://gitlab.com/puravida-software/groogle/commit/662bb7c0be9883b) jorge *2018-01-29 21:43:50*

**sheets a 1.3.1**


[d02c9a19a90fb45](https://gitlab.com/puravida-software/groogle/commit/d02c9a19a90fb45) jorge *2018-01-29 21:28:28*

**sheets a 1.3.1**


[6c26eb4de2cfb41](https://gitlab.com/puravida-software/groogle/commit/6c26eb4de2cfb41) jorge *2018-01-29 21:25:42*

**sheets a 1.3.1**


[4818cd1eb640279](https://gitlab.com/puravida-software/groogle/commit/4818cd1eb640279) jorge *2018-01-29 21:18:59*

**sheets a 1.3.1**


[7db9c2835acf4d1](https://gitlab.com/puravida-software/groogle/commit/7db9c2835acf4d1) jorge *2018-01-29 20:57:50*

**sheets a 1.3.1**


[6ac2ddab4cd0273](https://gitlab.com/puravida-software/groogle/commit/6ac2ddab4cd0273) jorge *2018-01-29 20:49:50*

**sheets a 1.3.1**


[ec4fc4c531c3d58](https://gitlab.com/puravida-software/groogle/commit/ec4fc4c531c3d58) jorge *2018-01-29 20:47:31*

**ci test for 1.3.1**


[735e4872f3e5cf5](https://gitlab.com/puravida-software/groogle/commit/735e4872f3e5cf5) jorge *2018-01-29 19:55:17*

**aproximacion a lambdas**


[b005b0f16684a7c](https://gitlab.com/puravida-software/groogle/commit/b005b0f16684a7c) jorge *2018-01-29 19:42:26*

**aproximacion a lambdas**


[e8feaa7c1264a06](https://gitlab.com/puravida-software/groogle/commit/e8feaa7c1264a06) jorge *2018-01-24 08:37:33*

**improvements**


[f71d712a748fed6](https://gitlab.com/puravida-software/groogle/commit/f71d712a748fed6) jorge *2018-01-22 21:54:40*

**some bugs, improvement and docu**


[f6d7adbc2646ebd](https://gitlab.com/puravida-software/groogle/commit/f6d7adbc2646ebd) jorge *2018-01-22 21:50:41*

**withFiles test in java**


[cecc4a4cfff7eb5](https://gitlab.com/puravida-software/groogle/commit/cecc4a4cfff7eb5) jorge *2018-01-21 17:00:39*

**Testing Java**


[ec9038e63a93f7c](https://gitlab.com/puravida-software/groogle/commit/ec9038e63a93f7c) jorge *2018-01-21 09:08:08*

**mejora docu core**


[fdc492d5161d848](https://gitlab.com/puravida-software/groogle/commit/fdc492d5161d848) jorge *2018-01-20 17:41:45*

**start 1.3.0**


[74a439cfea91100](https://gitlab.com/puravida-software/groogle/commit/74a439cfea91100) Jorge Aguilera *2018-01-10 16:14:31*

**groogle-core docu**


[7c1f9e9c0a0b7c2](https://gitlab.com/puravida-software/groogle/commit/7c1f9e9c0a0b7c2) jorge *2018-01-09 21:56:14*

**groogle-core docu**


[f6c8611f1c4d849](https://gitlab.com/puravida-software/groogle/commit/f6c8611f1c4d849) jorge *2018-01-09 21:54:13*

**groogle-core docu**


[d2dbc7192058206](https://gitlab.com/puravida-software/groogle/commit/d2dbc7192058206) jorge *2018-01-09 21:47:40*

**groogle-core docu**


[4714c340f20a384](https://gitlab.com/puravida-software/groogle/commit/4714c340f20a384) jorge *2018-01-09 21:38:51*

**groogle-core docu**


[ced460fd1c74827](https://gitlab.com/puravida-software/groogle/commit/ced460fd1c74827) jorge *2018-01-09 21:32:29*

**v1.2.0**

 * groogle-sheet + groogle-drive

[2dced0c8dd45a4c](https://gitlab.com/puravida-software/groogle/commit/2dced0c8dd45a4c) jorge *2018-01-09 21:20:02*


## Tag v1.3.1
### No issue

**typo before release 1.3.1**


[932f56953f84299](https://gitlab.com/puravida-software/groogle/commit/932f56953f84299) jorge *2018-01-29 19:49:57*


## Tag v1.2.1
### No issue

**groogle-drive test**


[6f773f249e6c829](https://gitlab.com/puravida-software/groogle/commit/6f773f249e6c829) jorge *2018-01-20 17:17:14*


## Tag v1.1.0
### No issue

**v1.1.0**

 * groogle-sheet + docu

[58d618561133f34](https://gitlab.com/puravida-software/groogle/commit/58d618561133f34) jorge *2017-12-25 19:46:56*

**una primera docu de groogle-sheet**


[3380eedd40283a1](https://gitlab.com/puravida-software/groogle/commit/3380eedd40283a1) jorge *2017-12-23 23:46:22*

**nuevo modulo groogle-sheet**

 * documentacion asciidoctor para los dos proyectos

[d7a0602c88e4459](https://gitlab.com/puravida-software/groogle/commit/d7a0602c88e4459) jorge *2017-12-23 23:39:27*

**nuevo modulo groogle-sheet**

 * documentacion asciidoctor para los dos proyectos

[82824cd2b15ae6a](https://gitlab.com/puravida-software/groogle/commit/82824cd2b15ae6a) jorge *2017-12-23 23:30:10*

**include google drive api**


[5ae2c1e64427b5c](https://gitlab.com/puravida-software/groogle/commit/5ae2c1e64427b5c) jorge *2017-12-03 11:08:50*



import com.google.api.services.drive.DriveScopes
import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.GroogleScript
import com.puravida.groogle.SheetScript

SheetScript.instance.with {
//tag::login[]
    login {
        applicationName 'groogle-example'

        withScopes DriveScopes.DRIVE

        usingCredentials '/client_secret.json'

        asService false
    }
//end::login[]

    //tag::listSheets[]
    spreadSheets.each{
        println "$it.name ($it.id)"
    }
    //end::listSheets[]
}
package com.puravida.groogle

import com.google.maps.GeoApiContext

class ConfigMapsSpec {

    String apiKey

    ConfigMapsSpec apiKey(String apiKey){
        assert clientId==null, "Can't set credentials with apiKey"
        this.apiKey=apiKey
        this
    }

    String clientId
    String clientSecret
    ConfigMapsSpec enterpriseCredentials(String clientId, String clientSecret){
        assert apiKey==null, "Can't set credentials with apiKey"
        this.clientId=clientId
        this.clientSecret=clientSecret
        this
    }

    Integer queryRateLimit
    ConfigMapsSpec queryRateLimit(int queryRateLimit){
        this.queryRateLimit=queryRateLimit
        this
    }

    GeoApiContext geoApiContext

    protected void execute(){
        GeoApiContext.Builder builder = new GeoApiContext.Builder()

        if(apiKey)
            builder.apiKey(apiKey)
        else
            builder.enterpriseCredentials(clientId,clientSecret)

        if( queryRateLimit)
            builder.queryRateLimit(queryRateLimit)

        geoApiContext = builder.build()
    }
}

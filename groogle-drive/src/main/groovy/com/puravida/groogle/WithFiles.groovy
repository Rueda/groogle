package com.puravida.groogle

import com.google.api.services.drive.Drive
import com.google.api.services.drive.model.File

import java.util.function.Consumer

class WithFiles {

    protected Drive service

    private String []corpora

    void findIn(String str){
        this.corpora=[str]
    }
    void findIn(String[] str){
        this.corpora=str
    }

    private Boolean includeTeamDriveItems = null
    void includeTeamDriveItems(Boolean b){
        this.includeTeamDriveItems=b
    }

    private int batchSize = 20
    void batchSize(int size){
        this.batchSize=size
    }

    private String orderBy
    void orderBy(String orderBy){
        this.orderBy=orderBy
    }

    private String nextPageToken

    private Closure eachFileClosure

    void eachFile(@DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=WithFile)Closure c){
        eachFileClosure = c
    }

    private List filters = []
    void nameEq(String str){
        filters.add "name = '$str'"
    }

    void nameStartWith(String str){
        filters.add "name contains '$str'"
    }

    void parentId(String str){
        filters.add "parents in '$str'"
    }


    private List<com.google.api.services.drive.model.File> processed = []

    protected List<com.google.api.services.drive.model.File> execute(){
        Drive.Files.List files = service.files().list()

        files.pageSize = batchSize
        if(corpora)
            files.setCorpora(corpora.join(','))
        if(includeTeamDriveItems != null)
            files.includeTeamDriveItems=includeTeamDriveItems
        if(orderBy)
            files.orderBy=orderBy
        if(nextPageToken)
            files.pageToken=nextPageToken
        if( filters.size())
            files.q = filters.join(' and ')

        files.fields="nextPageToken,files(id,name,parents,mimeType)"

        def response = files.execute()
        nextPageToken = response.nextPageToken
        List<com.google.api.services.drive.model.File> ret = response.files
        ret.each {
            WithFile withFile = new WithFile(service: service, file: it)
            Closure c = eachFileClosure.rehydrate(withFile,withFile,withFile)
            c.call(it)
        }
        processed.addAll(ret)
        if(nextPageToken){
            execute()
        }
        processed
    }

}

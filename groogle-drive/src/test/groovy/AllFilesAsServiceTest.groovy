import com.google.api.services.drive.DriveScopes
import com.puravida.groogle.DriveScript
import com.puravida.groogle.GroogleScript

//tag::script[]
DriveScript.instance.with {

    login{
        applicationName 'groogle-example'

        withScopes DriveScopes.DRIVE

        usingCredentials '/groogle-688bcfc07d1b.json'

        asService true
    }

    allFiles().each {
        println "$it.id $it.name"
    }
}
//end::script[]

package groogle.rest

import com.google.api.services.drive.DriveScopes
import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.GroogleScript
import com.puravida.groogle.SheetScript

class BootStrap {

    def init = { servletContext ->
        GroogleScript.instance.login{

            applicationName 'groogle-example'

            withScopes DriveScopes.DRIVE, SheetsScopes.SPREADSHEETS

            usingCredentials '/groogle-688bcfc07d1b.json'

            asService true
        }
        SheetScript.instance.groogleScript=GroogleScript.instance
    }
    def destroy = {
    }
}

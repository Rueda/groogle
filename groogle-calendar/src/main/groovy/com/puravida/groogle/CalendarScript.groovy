package com.puravida.groogle

import com.google.api.client.googleapis.json.GoogleJsonResponseException
import com.google.api.services.calendar.Calendar
import com.google.api.services.calendar.model.CalendarList

import java.util.function.Consumer

@Singleton
class CalendarScript extends AbstractScript{

    protected Calendar service

    AbstractScript initScript(GroogleScript groogleScript){
        this.service = new Calendar.Builder(groogleScript.httpTransport, groogleScript.jsonFactory, groogleScript.credential)
                .setApplicationName(groogleScript.applicationName)
                .build();
        this
    }

    List<CalendarBean> allCalendars(String restriction=null,
                           boolean showDeleted=false,
                           boolean showHidden=false)throws IOException{
        assert groogleScript != null, "Need to obtain credentials. Try to use login method"
        def ret = []
        def request = service.calendarList().list()

        if(restriction) request.minAccessRole = restriction
        if(showDeleted) request.showDeleted = showDeleted
        if(showHidden) request.showHidden = showHidden

        def response = request.execute()
        while (true){
            ret.addAll response.items.collect{ CalendarBean.fromRequest(it)}
            if(!response.nextPageToken)
                break

            request.pageToken = response.nextPageToken
            response = request.execute()
        }
        ret
    }

    void removeCalendarFromUserList(String calendarId){
        try{
            service.calendarList().delete(calendarId).execute()
        }catch (GoogleJsonResponseException ge){
            if( ge.getStatusCode()!=404)
                throw ge
        }
    }

    CalendarBean addCalendarToUsersList(String calendarId){
        CalendarList cl = new CalendarList(id:calendarId)
        CalendarBean.fromCalendarList(service.calendarList().insert(cl).execute())
    }


    void withCalendar(String calendarId, Consumer<WithCalendar> consumer){
        withCalendar(calendarId,{ WithCalendar withCalendar->
            consumer.accept(withCalendar)
        })
    }

    void withCalendar( String calendarId, @DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=WithCalendar)Closure closure){
        assert groogleScript != null, "Need to obtain credentials. Try to use login method"
        WithCalendar withCalendar = new WithCalendar(calendarId:calendarId,service: service)
        Closure c = closure.rehydrate(withCalendar,withCalendar,withCalendar)
        c.call(withCalendar)
        withCalendar.execute()
    }

    void createEvent(String calendarId, Consumer<CreateEvent> consumer){
        createEvent(calendarId,{
            consumer.accept(it)
        })
    }

    void createEvent(String calendarId, @DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=CreateEvent)Closure closure){
        CreateEvent createEvent = new CreateEvent(service:service,calendarId:calendarId)
        Closure cl = closure.rehydrate(createEvent,createEvent,createEvent)
        cl.call(createEvent)
        createEvent.execute()
    }
}
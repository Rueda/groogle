package com.puravida.groogle

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory
import com.google.api.client.util.store.AbstractDataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory
import groovy.transform.CompileStatic;

@Singleton
@CompileStatic
class GroogleScript {

    JsonFactory jsonFactory = JacksonFactory.defaultInstance
    HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport()

    String applicationName = 'GroogleScript'

    File getDataStoreDir(){
        new File(System.getProperty("user.home"), ".credentials/${applicationName}")
    }

    AbstractDataStoreFactory getDataStoreFactory(){
        new FileDataStoreFactory(dataStoreDir)
    }

    Credential credential

    GroogleScript login(InputStream clientSecret, List<String> scopes) {
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jsonFactory, new InputStreamReader(clientSecret))

        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(
                        httpTransport, jsonFactory, clientSecrets, scopes)
                        .setDataStoreFactory(dataStoreFactory)
                        .setAccessType("offline")
                        .build()

        credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user")
        this
    }

    GroogleScript loginService(InputStream clientSecret, List<String> scopes) {
        credential = GoogleCredential.fromStream(clientSecret).createScoped(scopes)
        this
    }

    GroogleScript login(@DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=LoginSpec)Closure closure){
        LoginSpec login = new LoginSpec()
        Closure clone = closure.rehydrate(login,login,login)
        clone.resolveStrategy = Closure.DELEGATE_ONLY
        clone()
        login.execute()
        this
    }

    @Deprecated
    void evaluate( File txt, Object delegate=null ){
        evaluate( txt.text, delegate)
    }

    @Deprecated
    void evaluate( String txt, Object delegate=null ){
        String evaluate = """{ script-> script.with{ $txt } }"""
        Closure closure = new GroovyShell().evaluate(evaluate) as Closure
        Closure cl = closure.rehydrate(delegate ?: this, delegate ?: this, delegate ?: this)
        cl.resolveStrategy = Closure.DELEGATE_ONLY
        cl()
    }
}
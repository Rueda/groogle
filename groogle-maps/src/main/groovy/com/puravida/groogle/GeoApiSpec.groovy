package com.puravida.groogle

import com.google.maps.GeoApiContext
import com.google.maps.GeocodingApi
import com.google.maps.GeocodingApiRequest
import com.google.maps.model.GeocodingResult
import com.google.maps.model.LatLng
import groovy.transform.CompileStatic

@CompileStatic
class GeoApiSpec implements IndicationTrait{

    protected GeoApiContext geoApiContext

    protected Closure eachResult
    GeoApiSpec eachResult(@DelegatesTo(strategy = Closure.DELEGATE_ONLY, value = GeocodingResult) Closure closure) {
        this.eachResult = closure.clone() as Closure
        this
    }

    protected Closure reverseResult
    GeoApiSpec reverseResult(@DelegatesTo(strategy = Closure.DELEGATE_ONLY, value = GeocodingResult) Closure closure) {
        this.reverseResult = closure.clone()as Closure
        this
    }

    String usingIsoCode
    GeoApiSpec usingIsoCode(String usingIsoCode){
        this.usingIsoCode=usingIsoCode
        this
    }

    Double[] bounds
    GeoApiSpec bounds( double latSW, double longSW, double latNE, double longNE){
        bounds = [latSW, longSW, latNE, longNE]
        this
    }

    String region
    GeoApiSpec region(String region){
        this.region = region
        this
    }

    protected GeocodingResult[] execute() {
        GeocodingResult[] results

        GeocodingApiRequest request = new GeocodingApiRequest(geoApiContext)

        if(usingIsoCode)
            request.language(usingIsoCode)

        if(bounds)
            request.bounds(new LatLng(bounds[0],bounds[1]),new LatLng(bounds[2],bounds[3]))

        if( region )
            request.region(region)

        if(address) {
            results = request.address(address).await()
        }
        if( latitud && longitud){
            results = request.latlng(new LatLng(latitud,longitud)).await()
        }
        if( placeId ){
            results = request.place(placeId).await()
        }

        if( eachResult ) {
            results.each {
                Closure clone = eachResult.rehydrate(it, it, it)
                clone.resolveStrategy = Closure.DELEGATE_ONLY
                clone()
            }
        }
        if( reverseResult ) {
            results.reverseEach {
                Closure clone = reverseResult.rehydrate(it, it, it)
                clone.resolveStrategy = Closure.DELEGATE_ONLY
                clone()
            }
        }

        results
    }
}

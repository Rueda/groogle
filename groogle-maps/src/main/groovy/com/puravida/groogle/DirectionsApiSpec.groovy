package com.puravida.groogle

import com.google.maps.DirectionsApiRequest
import com.google.maps.GeoApiContext
import com.google.maps.model.DirectionsResult
import com.google.maps.model.GeocodingResult
import com.google.maps.model.LatLng
import com.google.maps.model.TravelMode
import groovy.time.TimeCategory
import groovy.transform.CompileStatic
import org.joda.time.DateTime
import org.joda.time.ReadableInstant

@CompileStatic
class DirectionsApiSpec {

    protected GeoApiContext geoApiContext

    protected TravelMode mode = TravelMode.DRIVING

    protected IndicationSpec from
    DirectionsApiSpec from(@DelegatesTo(strategy = Closure.DELEGATE_ONLY, value = IndicationSpec)Closure closure){
        from = new IndicationSpec()
        Closure clone = closure.rehydrate(from, from, from)
        clone.resolveStrategy=Closure.DELEGATE_ONLY
        clone()
        this
    }
    DirectionsApiSpec from(String address){
        from = new IndicationSpec(address: address)
        this
    }
    DirectionsApiSpec from(Double lat, Double lng){
        from = new IndicationSpec(latitud: lat, longitud: lng)
        this
    }

    protected IndicationSpec to
    DirectionsApiSpec to(@DelegatesTo(strategy = Closure.DELEGATE_ONLY, value = IndicationSpec)Closure closure){
        to = new IndicationSpec()
        Closure clone = closure.rehydrate(to, to, to)
        clone.resolveStrategy=Closure.DELEGATE_ONLY
        clone()
        this
    }
    DirectionsApiSpec to(String address){
        to = new IndicationSpec(address: address)
        this
    }
    DirectionsApiSpec to(Double lat, Double lng){
        to = new IndicationSpec(latitud: lat, longitud: lng)
        this
    }

    Date getNow(){
        new Date()
    }

    Date getTomorrow(){
        new Date()+1
    }

    Date getInOneHour(){
        Date ret = new Date()
        ret[Calendar.HOUR_OF_DAY]+=1
        ret
    }

    protected ReadableInstant departureTime

    DirectionsApiSpec departure(int year, int monthOfYear, int dayOfMonth, int hourOfDay=0, int minuteOfHour=0, int secondOfMinute=0){
        departureTime = new DateTime(year, monthOfYear, dayOfMonth, hourOfDay, minuteOfHour, secondOfMinute)
        this
    }

    DirectionsApiSpec departure(Date when){
        departureTime = new DateTime(when.time)
        this
    }

    protected ReadableInstant arrivalTime

    DirectionsApiSpec arrival(int year, int monthOfYear, int dayOfMonth, int hourOfDay=0, int minuteOfHour=0, int secondOfMinute=0){
        arrivalTime = new DateTime(year, monthOfYear, dayOfMonth, hourOfDay, minuteOfHour, secondOfMinute)
        this
    }

    DirectionsApiSpec arrival(Date when){
        arrivalTime = new DateTime(when.time)
        this
    }

    protected List<String> wayPointsStr = []
    DirectionsApiSpec wayPoints(String ...waysPoints){
        wayPointsStr.addAll waysPoints
        this
    }

    DirectionsApiSpec wayPoint(String waysPoint){
        wayPointsStr.addAll waysPoint
        this
    }
    protected List<LatLng> wayPointsLatLng = []
    DirectionsApiSpec wayPoint(Double lat, Double lng){
        wayPointsLatLng.add( new LatLng(lat,lng))
        this
    }

    protected DirectionsResult execute() {
        assert from != null, "From required"
        assert to != null, "To required"

        DirectionsApiRequest directionsApiRequest = new DirectionsApiRequest(geoApiContext)

        if( from.address )
            directionsApiRequest.origin(from.address)
        if( from.latitud )
            directionsApiRequest.origin(from.latLng())
        if( from.placeId)
            directionsApiRequest.origin(from.placeId)

        if( to.address )
            directionsApiRequest.destination(to.address)
        if( to.latitud )
            directionsApiRequest.destination(to.latLng())
        if( to.placeId)
            directionsApiRequest.destination(to.placeId)

        directionsApiRequest.mode(mode)

        if( departureTime)
            directionsApiRequest.departureTime(departureTime)
        if( arrivalTime )
            directionsApiRequest.arrivalTime(arrivalTime)

        if( wayPointsStr.size())
            directionsApiRequest.waypoints(wayPointsStr as String[])
        if( wayPointsLatLng.size())
            directionsApiRequest.waypoints(wayPointsLatLng as LatLng[])

        DirectionsResult result = directionsApiRequest.await()

        result
    }

}

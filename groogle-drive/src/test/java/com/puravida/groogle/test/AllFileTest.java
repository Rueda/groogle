package com.puravida.groogle.test;

import com.google.api.services.drive.model.File;
import com.puravida.groogle.*;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;


public class AllFileTest {

    @Before
    public void doLogin(){
        //tag::login[]
        InputStream clientSecret = getClass().getResourceAsStream("/client_secret.json");
        GroogleScript groogleScript = GroogleScript.getInstance();
        groogleScript.setApplicationName("groogle-example");
        groogleScript.login(clientSecret, Arrays.asList("https://www.googleapis.com/auth/drive"));

        DriveScript.instance.setGroogleScript(groogleScript);
        //end::login[]
    }

    @Test
    public void allFiles() throws IOException{
        //tag::script[]
        for( File f : DriveScript.instance.allFiles()){
            System.out.println(f.getId()+" "+f.getName()+" "+f.getParents());
        };
        //end::script[]
    }


}

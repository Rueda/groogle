import com.google.api.services.calendar.CalendarScopes
import com.puravida.groogle.GroogleScript
import com.puravida.groogle.CalendarScript
import com.puravida.groogle.WithCalendar
import com.puravida.groogle.WithEvent

CalendarScript.instance.with{
    //tag::login[]
    login {
        applicationName 'groogle-example'
        withScopes CalendarScopes.CALENDAR
        usingCredentials '/client_secret.json'
        asService false
    }
    //end::login[]


    //tag::list[]
    allCalendars().each{
        println "Calendar id $it.id"
    }
    //end::list[]

    //tag::withCalendar[]
    String groogleCalendarId="groovy.groogle@gmail.com"

    withCalendar  groogleCalendarId,{ WithCalendar withCalendar->

        batchSize 20

        eachEvent{ WithEvent withEvent->
            println "Evento $withEvent.event.summary"

            withEvent.event.summary = "modificado ${new Date()}"

            boolean fullday = true
            if(fullDay)
                moveTo new Date().format('yyyy-MM-dd')
            else
                moveTo new Date()
        }
    }
    //end::withCalendar[]

    //tag::remove
    removeCalendarFromUserList"este id no existe"
    //end::remove

}

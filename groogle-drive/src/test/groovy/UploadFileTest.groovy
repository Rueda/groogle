import com.google.api.services.drive.DriveScopes
import com.puravida.groogle.DriveScript
import com.puravida.groogle.GroogleScript

//tag::script[]
DriveScript.instance.with {
    //tag::login[]
    login{
        applicationName 'groogle-example'

        withScopes DriveScopes.DRIVE

        usingCredentials '/client_secret.json'

        asService false
    }
    //end::login[]

    def f1 = uploadFile {
        name 'hola.txt'
        mimeType 'text/plain'
        content new ByteArrayInputStream('hola caracola'.bytes) //<1>
    }
    println f1.id

    def f2 = uploadFile{
        name 'hola_caracola.csv'
        mimeType 'text/csv'
        content new ByteArrayInputStream('h1,h2\n100,200'.bytes)
        saveAs GoogleSheet      //<2>
    }
    println f2.id

    def f3 = uploadFile{
        content new File('test.docx') //<3>
        saveAs GoogleDoc   //<4>
    }
    println f3.id
}
//end::script[]

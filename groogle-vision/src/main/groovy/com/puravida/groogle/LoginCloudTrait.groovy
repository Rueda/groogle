package com.puravida.groogle

trait LoginCloudTrait {

    private String applicationName
    LoginCloudTrait applicationName(String name){
        applicationName  = name
        this
    }

    private InputStream clientSecret
    InputStream getClientSecret(){
        this.clientSecret
    }

    LoginCloudTrait usingCredentials(String fileName){
        File f = new File(fileName)
        if( f.exists()  ){
            usingCredentials(f)
        }else{
            InputStream resource = this.class.getResourceAsStream(fileName)
            if( resource ){
                usingCredentials(resource)
            }else{
                throw new RuntimeException("Credentials $fileName not found")
            }
        }
    }

    LoginCloudTrait usingCredentials(File clientSecret){
        usingCredentials(clientSecret.newInputStream())
    }

    LoginCloudTrait usingCredentials(InputStream clientSecret){
        this.clientSecret=clientSecret
        this
    }


}

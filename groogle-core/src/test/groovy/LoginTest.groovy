//tag::test[]

import com.google.api.services.calendar.CalendarScopes
import com.google.api.services.drive.DriveScopes
import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.GroogleScript

GroogleScript.instance.applicationName='groogle-example'
clientSecret = this.class.getResource('client_secret.json').newInputStream()
GroogleScript.instance.login(clientSecret,[CalendarScopes.CALENDAR,
                                           DriveScopes.DRIVE,
                                           SheetsScopes.SPREADSHEETS])
//end::test[]

import com.google.api.services.drive.DriveScopes
import com.puravida.groogle.DriveScript
import com.puravida.groogle.GroogleScript

//tag::script[]
DriveScript.instance.with {
    //tag::login[]
    login{
        applicationName 'groogle-example'

        withScopes DriveScopes.DRIVE

        usingCredentials '/client_secret.json'

        asService false
    }
    //end::login[]

    withFiles {
        nameEq 'Facturae.xml'
        batchSize 2
        eachFile { file ->
            def newContent = new String(content.toByteArray())
            newContent = newContent.reverse()
            save( new ByteArrayInputStream(newContent.getBytes('UTF-8')) ) //<1>
        }
    }

    withFiles {
        nameEq 'Facturae.xml'
        batchSize 2
        eachFile { file ->
            println new String(content.toByteArray()) //<2>
        }
    }
}
//end::script[]

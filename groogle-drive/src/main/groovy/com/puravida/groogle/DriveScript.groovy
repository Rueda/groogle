package com.puravida.groogle

import com.google.api.services.drive.Drive
import com.google.api.services.drive.model.File

import java.util.function.Consumer

@Singleton
class DriveScript extends AbstractScript{

    protected Drive service

    AbstractScript initScript(GroogleScript groogleScript){
        this.service = new Drive.Builder(groogleScript.httpTransport, groogleScript.jsonFactory, groogleScript.credential)
                .setApplicationName(groogleScript.applicationName)
                .build();
        this
    }

    List<com.google.api.services.drive.model.File>  getAllFiles() throws IOException {
        allFiles(null)
    }

    List<com.google.api.services.drive.model.File>  allFiles(String mimeType=null) throws IOException {
        assert groogleScript != null, "Need to obtain credentials. Try to use login method"
        def ret = []
        def request = service.files().list()
        request.fields="nextPageToken,files(id,name,parents,mimeType)"
        def response = request.execute()
        while (true){
            ret.addAll response.files.findAll { com.google.api.services.drive.model.File file ->
                if (mimeType)
                    file.mimeType == mimeType
                else
                    true
            }
            if(!response.nextPageToken)
                break
            request.pageToken = response.nextPageToken
            response = request.execute()
        }
        ret
    }

    List<com.google.api.services.drive.model.File>  getAllFolders() throws IOException{
        allFiles('application/vnd.google-apps.folder')
    }

    List<com.google.api.services.drive.model.File>  withFiles(Consumer<WithFiles> withFilesConsumer,
                                                              Consumer<com.google.api.services.drive.model.File>consumer)
            throws IOException {
        WithFiles withFiles = new WithFiles(service: service)
        withFilesConsumer.accept(withFiles)
        withFiles.eachFile {
            consumer.accept(it)
        }
        withFiles.execute()
    }

    List<com.google.api.services.drive.model.File>  withFiles(
            @DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=WithFiles)Closure closure) throws IOException{
        WithFiles whithFiles = new WithFiles(service: service)
        Closure c = closure.rehydrate(whithFiles, whithFiles, whithFiles)
        c.call(whithFiles )
        whithFiles.execute()
    }

    com.google.api.services.drive.model.File findFile(String fileId){
        assert groogleScript != null, "Need to obtain credentials. Try to use login method"

        def request = service.files().get(fileId)
        request.fields="id,name,parents"
        File file = request.execute()
        file
    }

    void withFile( String fileId, Consumer<WithFile>consumer) {
        assert groogleScript != null, "Need to obtain credentials. Try to use login method"
        try {
            def request = service.files().get(fileId)
            request.fields = "id,name,parents"
            File file = request.execute()

            assert file, "File $fileId, not found"
            withFile(file, { WithFile withFile ->
                consumer.accept(withFile)
            })
        }catch (e){

        }
    }

    void withFile( String fileId, @DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=WithFile)Closure closure){
        assert groogleScript != null, "Need to obtain credentials. Try to use login method"
        File file = service.files().get(fileId).execute()
        assert file, "File $fileId, not found"
        withFile(file,closure)
    }

    void withFile( File file, @DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=WithFile)Closure closure){
        assert groogleScript != null, "Need to obtain credentials. Try to use login method"
        WithFile withFile = new WithFile(file: file, service: service)
        Closure c = closure.rehydrate(withFile,withFile,withFile)
        c.call(withFile)
        withFile.execute()
    }

    com.google.api.services.drive.model.File uploadFile(Consumer<CreateFile>consumer) throws IOException{
        uploadFile({ CreateFile createFile->
            consumer.accept(createFile)
        })
    }

    com.google.api.services.drive.model.File uploadFile(
            @DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=CreateFile)Closure closure) throws IOException{
        assert groogleScript != null, "Need to obtain credentials. Try to use login method"
        CreateFile createFile = new CreateFile(service: service)
        Closure c = closure.rehydrate(createFile,createFile,createFile)
        c.call(createFile)
        createFile.execute()
    }

    com.google.api.services.drive.model.File createFolder(String name, com.google.api.services.drive.model.File parent) throws IOException {
        assert groogleScript != null, "Need to obtain credentials. Try to use login method"
        createFolder(name,parent.id)
    }

    com.google.api.services.drive.model.File createFolder(String name, String parent=null) throws IOException{
        assert groogleScript != null, "Need to obtain credentials. Try to use login method"
        com.google.api.services.drive.model.File file = new com.google.api.services.drive.model.File()
        file.name=name
        file.mimeType='application/vnd.google-apps.folder'
        if(parent)
            file.parents = parent
        def request = service.files().create(file)
        request.fields='id,name,parents'
        request.execute()
    }

    List<java.io.File> downloadFolderWithName(String name, java.io.File into){
        List<java.io.File> ret = []
        com.google.api.services.drive.model.File file = allFolders.find{ it.name == name }
        if(file){
            return downloadFolder(file,into,ret)
        }
        ret
    }

    List<java.io.File> downloadFolderById(String id, java.io.File into){
        List<java.io.File> ret = []
        com.google.api.services.drive.model.File file = findFile(id)
        if(file){
            return downloadFolder(file,into,ret)
        }
        ret
    }

    protected List<java.io.File> downloadFolder(
            com.google.api.services.drive.model.File folder,
            java.io.File into,
            List<java.io.File>append){
        com.puravida.groogle.DriveScript me = this
        withFiles {
            parentId folder.id
            eachFile {
                if( file.mimeType == 'application/vnd.google-apps.folder' ){
                    java.io.File subfolder = new java.io.File(file.name,into)
                    subfolder.mkdirs()
                    me.downloadFolder(file,subfolder,append)
                }else{
                    java.io.File downloaded = new java.io.File(file.name,into)
                    downloaded.bytes = content.toByteArray()
                    append.add downloaded
                }
            }
        }

        append
    }

}
import com.google.api.services.drive.DriveScopes
import com.puravida.groogle.DriveScript
import com.puravida.groogle.GroogleScript

//tag::script[]
DriveScript.instance.with {
    //tag::login[]
    login{
        applicationName 'groogle-example'

        withScopes DriveScopes.DRIVE

        usingCredentials '/client_secret.json'

        asService false
    }
    //end::login[]

    withFiles {
        nameEq 'Facturae.xml'
        batchSize 2
        eachFile {
            title = "${new Date()}"     //<1>
            save()
        }
    }

    withFiles {
        nameEq 'Facturae.xml'
        batchSize 2
        eachFile {
            println title //<2>
        }
    }
}
//end::script[]

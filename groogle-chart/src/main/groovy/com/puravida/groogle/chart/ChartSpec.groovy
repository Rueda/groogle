package com.puravida.groogle.chart

import com.puravida.groogle.chart.core.BaseChartSpec
import groovy.util.logging.Log

@Log
class ChartSpec {

    BaseChartSpec baseChartSpec

    int width = 500
    int height = 200

    protected ChartSpec(){

    }

    private ChartSpec buildChart(@DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=BaseChartSpec)Closure closure){
        def code = closure.rehydrate(baseChartSpec ,baseChartSpec,baseChartSpec)
        code.resolveStrategy = Closure.DELEGATE_ONLY
        code()
        this
    }

    ChartSpec pieChart(@DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=PieChartSpec) Closure closure) {
        baseChartSpec = new PieChartSpec()
        buildChart closure
    }

    ChartSpec pie3DChart(@DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=PieChartSpec) Closure closure){
        baseChartSpec = new Pie3DChartSpec()
        buildChart closure
    }

    ChartSpec simpleLineChart(@DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=SimpleLineChartSpec) Closure closure){
        baseChartSpec = new SimpleLineChartSpec()
        buildChart closure
    }

    ChartSpec lineChart(@DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=LineChartSpec) Closure closure){
        baseChartSpec = new LineChartSpec()
        buildChart closure
    }

}

package com.puravida.groogle.chart

import com.puravida.groogle.chart.core.BaseChartSpec
import com.puravida.groogle.chart.core.AxiTrait

class LineChartSpec  extends BaseChartSpec implements AxiTrait{

    String getTYPE(){
        'lxy'
    }
}

import com.puravida.groogle.VisionScript

VisionScript.instance.with{

    applicationName 'groogle-example'

    usingCredentials '/groogle-688bcfc07d1b.json'

    //this.class.getResourceAsStream('/periodo4-laoconte.jpg')
    parseImages {

        image '/periodo4-laoconte.jpg', label, face, text

        image 'https://pbs.twimg.com/profile_images/945371516186873856/dzKiMQMB_400x400.jpg', text

    }
}
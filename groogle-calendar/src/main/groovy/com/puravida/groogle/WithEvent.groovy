package com.puravida.groogle

import com.google.api.client.util.DateTime
import com.google.api.services.calendar.Calendar
import com.google.api.services.calendar.model.Event
import com.google.api.services.calendar.model.EventDateTime

class WithEvent {

    protected Calendar service

    protected String calendarId

    protected Event event
    Event getEvent(){
        event
    }

    def getProperty(String propertyName){
        def metaProp = this.metaClass.getMetaProperty(propertyName)
        if( metaProp )
            return metaProp.getProperty(this)
        this.event."$propertyName"
    }

    void setProperty(String propertyName, Object newValue){
        def metaProp = this.metaClass.getMetaProperty(propertyName)
        if( metaProp ) {
            metaProp.setProperty(this, newValue)
            return
        }
        this.event."$propertyName" = newValue
    }

    void moveTo(String yyMMdd){
        event.end=event.start=new EventDateTime(date: new DateTime(yyMMdd))
    }

    void moveTo(Date date){
        event.end=event.start=new EventDateTime(dateTime: new DateTime(date.time))
    }


    protected toRemove=false
    void removeFromCalendar(){
        toRemove=true
    }

    protected Event execute(){
        if(toRemove) {
            return service.events().delete(calendarId,event.id).execute()
        }
        event=service.events().update(calendarId,event.id,event).execute()
        event
    }
}

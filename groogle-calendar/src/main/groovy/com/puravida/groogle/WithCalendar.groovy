package com.puravida.groogle

import com.google.api.services.calendar.Calendar
import com.google.api.services.calendar.model.Event

import java.util.function.Consumer

class WithCalendar {

    protected Calendar service

    protected String calendarId

    void clear(){
        service.calendars().clear(calendarId).execute()
    }

    private int batchSize = 20
    void batchSize(int size){
        this.batchSize=size
    }

    private String orderBy
    void orderBy(String orderBy){
        this.orderBy=orderBy
    }

    private String nextPageToken

    private Closure eachFileClosure

    void eachEvent(Consumer<WithEvent>consumer){
        eachEvent{
            consumer.accept(it)
        }
    }

    void eachEvent(@DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=WithEvent)Closure c){
        eachFileClosure = c.clone()
    }

    private List<Event> processed = []

    protected List<Event> execute(){
        Calendar.Events.List list = service.events().list(calendarId)
        list.pageSize = batchSize
        if(orderBy)
            list.orderBy=orderBy
        if(nextPageToken)
            list.pageToken=nextPageToken

        def response = list.execute()
        nextPageToken = response.nextPageToken
        List<Event> ret = response.items
        ret.each{
            if(eachFileClosure) {
                WithEvent withEvent = new WithEvent(calendarId: calendarId, service: service, event: it)
                Closure c = eachFileClosure.rehydrate(withEvent,withEvent,withEvent)
                c.call(withEvent)
                withEvent.execute()
            }
        }
        processed.addAll(ret)
        if(nextPageToken){
            execute()
        }
        processed
    }
}

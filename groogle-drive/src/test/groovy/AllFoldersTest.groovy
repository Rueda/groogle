import com.google.api.services.drive.DriveScopes
import com.puravida.groogle.DriveScript
import com.puravida.groogle.GroogleScript

//tag::script[]
DriveScript.instance.with {
//tag::login[]
    login{
        applicationName 'groogle-example'

        withScopes DriveScopes.DRIVE

        usingCredentials '/client_secret.json'

        asService false
    }
//end::login[]

    String name
    allFolders.each {
        println "$it.id $it.name"
        name = it.name
    }

}
//end::script[]
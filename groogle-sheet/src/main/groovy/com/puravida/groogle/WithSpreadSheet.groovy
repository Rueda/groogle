package com.puravida.groogle

import com.google.api.services.sheets.v4.Sheets
import com.google.api.services.sheets.v4.model.Sheet

import java.util.function.Consumer

class WithSpreadSheet {

    Sheets service

    String spreadSheetId

    void withSheet(String nameSheet, Consumer<WithSheet>consumer){
        withSheet(nameSheet,{ WithSheet withSheet->
            consumer.accept(withSheet)
        })
    }

    void withSheet( String nameSheet, @DelegatesTo(strategy=Closure.DELEGATE_ONLY, value=WithSheet)Closure closure){
        WithSheet sheet = new WithSheet(spreadSheetId:spreadSheetId, sheetId:nameSheet, service: service)
        Closure c = closure.rehydrate(sheet,sheet,sheet)
        c.resolveStrategy = Closure.DELEGATE_FIRST
        c.call(sheet)
        sheet.execute()
    }

    List<Sheet> getSheets(){
        service.spreadsheets().get(spreadSheetId).setFields('sheets.properties').execute().sheets
    }

    void execute(){

    }

}
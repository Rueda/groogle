package com.puravida.groogle

import com.google.maps.PlacesApi
import com.google.maps.model.DirectionsResult
import com.google.maps.model.GeocodingResult
import com.google.maps.model.TravelMode
import groovy.transform.CompileStatic

@Singleton
@CompileStatic
class MapsScript {

    private ConfigMapsSpec loginMapsSpec = new ConfigMapsSpec()

    MapsScript config(@DelegatesTo(strategy = Closure.DELEGATE_ONLY, value = ConfigMapsSpec)Closure closure){
        Closure clone = closure.rehydrate(loginMapsSpec,loginMapsSpec,loginMapsSpec)
        clone.resolveStrategy=Closure.DELEGATE_ONLY
        clone()
        loginMapsSpec.execute()
        this
    }

    GeocodingResult[] geoLocalization(@DelegatesTo(strategy = Closure.DELEGATE_ONLY, value=GeoApiSpec)Closure closure){
        GeoApiSpec geoApiSpec = new GeoApiSpec(geoApiContext:loginMapsSpec.geoApiContext)
        Closure clone = closure.rehydrate(geoApiSpec,geoApiSpec,geoApiSpec)
        clone.resolveStrategy=Closure.DELEGATE_ONLY
        clone()
        geoApiSpec.execute()
    }

    DirectionsResult driving(@DelegatesTo(strategy = Closure.DELEGATE_ONLY, value=DirectionsApiSpec)Closure closure) {
        directions(TravelMode.DRIVING,closure)
    }
    DirectionsResult walking(@DelegatesTo(strategy = Closure.DELEGATE_ONLY, value=DirectionsApiSpec)Closure closure) {
        directions(TravelMode.WALKING,closure)
    }
    DirectionsResult bicycling(@DelegatesTo(strategy = Closure.DELEGATE_ONLY, value=DirectionsApiSpec)Closure closure) {
        directions(TravelMode.BICYCLING,closure)
    }
    DirectionsResult transit(@DelegatesTo(strategy = Closure.DELEGATE_ONLY, value=DirectionsApiSpec)Closure closure) {
        directions(TravelMode.TRANSIT,closure)
    }

    protected DirectionsResult directions(TravelMode travelMode, @DelegatesTo(strategy = Closure.DELEGATE_ONLY, value=DirectionsApiSpec)Closure closure){
        DirectionsApiSpec directionsApiSpec = new DirectionsApiSpec(geoApiContext: loginMapsSpec.geoApiContext,mode: travelMode)
        Closure clone = closure.rehydrate(directionsApiSpec,directionsApiSpec,directionsApiSpec)
        clone.resolveStrategy=Closure.DELEGATE_ONLY
        clone()
        directionsApiSpec.execute()
    }

    void places(@DelegatesTo(strategy = Closure.DELEGATE_ONLY, value=PlacesApiSpec)Closure closure){
        PlacesApiSpec placesApiSpec = new PlacesApiSpec(geoApiContext: loginMapsSpec.geoApiContext)
        Closure clone = closure.rehydrate(placesApiSpec,placesApiSpec,placesApiSpec)
        clone.resolveStrategy=Closure.DELEGATE_ONLY
        clone()
        placesApiSpec.execute()
    }

}

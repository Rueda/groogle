package com.puravida.groogle.test;

import com.google.api.services.drive.model.File;
import com.puravida.groogle.*;
import groovy.lang.Closure;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;


public class PrintWithFileTest {

    @Before
    public void doLogin() throws IOException{
        InputStream clientSecret = getClass().getResourceAsStream("/client_secret.json");
        GroogleScript groogleScript = GroogleScript.getInstance();
        groogleScript.setApplicationName("groogle-example");
        groogleScript.login(clientSecret, Arrays.asList("https://www.googleapis.com/auth/drive"));

        DriveScript.instance.setGroogleScript(groogleScript);
    }

    @Test
    public void withFile()throws IOException{

        List<File> folders = DriveScript.instance.getAllFolders();
        File first = folders.get(0);

        File file = DriveScript.instance.uploadFile((CreateFile createFile)->{
            createFile.name("test.docx");
            createFile.saveAs(createFile.getGoogleDoc());
            createFile.content(getClass().getResourceAsStream("/test.docx"));
            createFile.intoFolder(first);
        });

        assert(file!=null);
        assert(file.getId()!=null);

        DriveScript.instance.withFile(file.getId(),
                (WithFile withFile)->{
                    System.out.println(withFile.getFile().getId()+" "+withFile.getFile().getName());
                    withFile.removeFromDrive();
                }
        );
    }

}

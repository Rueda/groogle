package com.puravida.groogle.chart

import spock.lang.Specification

class LineChartTestSpec extends Specification{

    def "simpleLineChart with 3 values"(){
        given:
        String fileName = "build/${specificationContext.currentSpec.name}_${specificationContext.currentIteration.name}.png"
        new File(fileName).mkdirs()
        new File(fileName).delete()

        and:
        ChartSpec chartSpec = new ChartSpec(width: 500,height: 100)
        Chart chart = new Chart(chartSpec)

        when:
        chartSpec.simpleLineChart{
            fixedValues {
                values 10,22.3,34,12,5,100
            }
        }

        then:
        chart.GET == 'https://chart.googleapis.com/chart?chs=500x100&cht=lc&chd=t:10.0,22.3,34.0,12.0,5.0,100.0'
        chart.bytes.length

        and:
        new File(fileName) << chart.bytes

        and:
        new File(fileName).exists()
    }


    def "xyLineChart with 3 values"(){
        given:
        String fileName = "build/${specificationContext.currentSpec.name}_${specificationContext.currentIteration.name}.png"
        new File(fileName).mkdirs()
        new File(fileName).delete()

        and:
        ChartSpec chartSpec = new ChartSpec(width: 500,height: 100)
        Chart chart = new Chart(chartSpec)

        when:
        chartSpec.lineChart{
            fixedValues {
                forEach 10,20,40,80,90 assign 20,30,12,50,34
            }
        }

        then:
        chart.GET == 'https://chart.googleapis.com/chart?chs=500x100&cht=lxy&chd=t:10,20,40,80,90|20,30,12,50,34'
        chart.bytes.length

        and:
        new File(fileName) << chart.bytes

        and:
        new File(fileName).exists()
    }

    def "2 xyLineChart with 3 values"(){
        given:
        String fileName = "build/${specificationContext.currentSpec.name}_${specificationContext.currentIteration.name}.png"
        new File(fileName).mkdirs()
        new File(fileName).delete()

        and:
        ChartSpec chartSpec = new ChartSpec(width: 500,height: 100)
        Chart chart = new Chart(chartSpec)

        when:
        chartSpec.lineChart{
            fixedValues {
                forEach 10,20,40,80,90 assign 20,30,12,50,34 withLabels '1','2','3','4'
            }
            fixedValues {
                forEach (-1) assign 30,10,20,50,64 withLabels 'A','b','c','d'
            }
        }

        then:
        chart.GET == 'https://chart.googleapis.com/chart?chs=500x100&cht=lxy&chd=t:10,20,40,80,90|20,30,12,50,34|-1|30,10,20,50,64&chl=1|2|3|4|A|b|c|d'
        chart.bytes.length

        and:
        new File(fileName) << chart.bytes

        and:
        new File(fileName).exists()
    }


}

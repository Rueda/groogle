
import com.google.api.services.calendar.CalendarScopes
import com.google.api.services.drive.DriveScopes
import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.GroogleScript

//tag::test[]
GroogleScript.instance.login{

    applicationName 'groogle-example'

    withScopes CalendarScopes.CALENDAR, DriveScopes.DRIVE, SheetsScopes.SPREADSHEETS

    usingCredentials '/groogle-688bcfc07d1b.json'

    asService true
}
//end::test[]



package groogle;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.drive.model.File;
import com.puravida.groogle.*;
import groovy.lang.Closure;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.function.Consumer;


public class DriveTest {

    @Before
    public void doLogin(){
        InputStream clientSecret = getClass().getResourceAsStream("/client_secret.json");
        GroogleScript groogleScript = GroogleScript.getInstance();
        groogleScript.setApplicationName("groogle-test");
        groogleScript.login(clientSecret, Arrays.asList("https://www.googleapis.com/auth/drive"));

        DriveScript.instance.setGroogleScript(groogleScript);
    }

    @Test
    public void allFiles() throws IOException{
        for( com.google.api.services.drive.model.File f : DriveScript.instance.allFiles()){
            System.out.println(f.getId()+" "+f.getName());
        };
    }


    @Test
    public void uploadFile()throws IOException{
    }

    @Test
    public void withFilesConsumers()throws IOException{
    }

    @Test
    public void withFile()throws IOException{
    }

}

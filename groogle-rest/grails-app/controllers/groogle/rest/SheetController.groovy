package groogle.rest


import grails.rest.*
import grails.converters.*
import com.puravida.groogle.SheetScript

class SheetController {
	static responseFormats = ['json', 'xml']
	
    def index() {
        def ret = [:]
        SheetScript.instance.listSpreadSheets.each{
            ret["$it.id"]="$it.name"
        }
        ret
    }

    def show(String id){
        SheetScript.instance.withSheet(id,{

        })
    }
}

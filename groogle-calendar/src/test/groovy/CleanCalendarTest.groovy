import com.google.api.client.util.DateTime
import com.google.api.services.calendar.CalendarScopes
import com.google.api.services.calendar.model.Calendar
import com.google.api.services.calendar.model.EventDateTime
import com.puravida.groogle.GroogleScript
import com.puravida.groogle.CalendarScript
import com.puravida.groogle.WithCalendar
import com.puravida.groogle.WithEvent

//tag::login[]
GroogleScript.instance.applicationName='groogle-example'
clientSecret = this.class.getResource('client_secret.json').newInputStream()
CalendarScript.instance.groogleScript=GroogleScript.instance.login(clientSecret,[CalendarScopes.CALENDAR])
//end::login[]

//tag::removeFromCalendar[]
String groogleCalendarId="groovy.groogle@gmail.com"
CalendarScript.instance.withCalendar( groogleCalendarId,{ WithCalendar withCalendar->
    batchSize(20)
    eachEvent{ WithEvent withEvent->
        println "Remove Evento $withEvent.event.summary"
        removeFromCalendar()
    }
})
//end::removeFromCalendar[]
package com.puravida.groogle.chart.core

abstract class BaseChartSpec implements TypeTrait{


    @Override
    String toString() {
        def params = [typeAsString]
        if( this instanceof ValuesTrait) {
            ValuesTrait vt = (ValuesTrait)this
            params += vt.valuesAsString
        }
        if( this instanceof AxiTrait) {
            AxiTrait vt = (AxiTrait)this
            params += vt.valuesAsString
        }
        params.findAll{it.length()}.join('&')
    }

    Object asType(Class clazz) {
        if( clazz == Map){
            Map params = typeAsMap
            if( this instanceof ValuesTrait) {
                ValuesTrait vt = (ValuesTrait)this
                params += vt.valuesAsMap
            }
            if( this instanceof AxiTrait) {
                AxiTrait vt = (AxiTrait)this
                params += vt.valuesAsMap
            }
            return params
        }
        this
    }

}

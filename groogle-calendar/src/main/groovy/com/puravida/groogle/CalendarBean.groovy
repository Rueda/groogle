package com.puravida.groogle

import com.google.api.services.calendar.model.CalendarList
import com.google.api.services.calendar.model.CalendarListEntry
import groovy.transform.ToString

@ToString
class CalendarBean {
    String id
    String summary
    String color
    String description
    String location
    boolean hidden
    boolean primarry
    boolean deleted

    protected static CalendarBean fromRequest(CalendarListEntry item){
        new CalendarBean(
                id:item.id,
                summary:item.summary,
                color:item.colorId,
                description:item.description,
                hidden:item.hidden ?:false,
                primarry:item.primary ?:false,
                deleted:item.deleted ?:false,
                location:item.location ?: ''
        )
    }

    protected static CalendarBean fromCalendarList(CalendarList item){
        new CalendarBean(
                id:item.id,
                summary:item.summary,
                color:item.colorId,
                description:item.description,
                hidden:item.hidden ?:false,
                primarry:item.primary ?:false,
                deleted:item.deleted ?:false,
                location:item.location ?: ''
        )
    }
}

package com.puravida.groogle.chart

import groovy.util.logging.Log
import org.apache.http.HttpResponse
import org.apache.http.NameValuePair
import org.apache.http.client.HttpClient
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpPost
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.message.BasicNameValuePair

@Log
class Chart {

    final static String URL='https://chart.googleapis.com/chart'

    ChartSpec chartSpec

    Chart(ChartSpec chartSpec){
        this.chartSpec = chartSpec
    }

    String getGET(){
        "$URL?chs=${chartSpec.width}x${chartSpec.height}&${chartSpec.baseChartSpec.toString()}"
    }

    byte[] getBytes(){
        if( GET.length() > 1024 )
            imageByPost
        else
            imageByGet
    }

    byte[] getImageByGet(){
        try{
            new URL(GET).bytes
        }catch(e){
            log.severe(e.toString())
            null
        }
    }


    byte[] getImageByPost(){
        try{
            HttpClient httpclient = HttpClientBuilder.create().build()
            HttpPost post = new HttpPost(URL)
            def params = [] as List<NameValuePair>
            def map = chartSpec.baseChartSpec as Map
            map.each{ kv ->
                params.add new BasicNameValuePair(kv.key,kv.value)
            }
            post.entity = new UrlEncodedFormEntity(params)
            HttpResponse response = httpclient.execute(post)
            response.entity.content.bytes
        }catch(e){
            e.printStackTrace()
            log.throwing('Chart','imageByPost',e)
            null
        }
    }

}

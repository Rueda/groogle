package com.puravida.groogle

import com.google.api.client.http.FileContent
import com.google.api.client.http.InputStreamContent
import com.google.api.services.drive.Drive
import com.google.api.services.drive.model.File

class WithFile {

    protected Drive service

    protected File file
    File getFile(){
        file
    }

    protected File fileMetadata = new File();
    File getMetaData(){
        fileMetadata
    }

    def getProperty(String propertyName){
        def metaProp = this.metaClass.getMetaProperty(propertyName)
        if( metaProp )
            return metaProp.getProperty(this)
        this.file."$propertyName"
    }

    void setProperty(String propertyName, Object newValue){
        def metaProp = this.metaClass.getMetaProperty(propertyName)
        if( metaProp ) {
            metaProp.setProperty(this, newValue)
            return
        }
        this.fileMetadata."$propertyName" = newValue
    }

    OutputStream getContent(){
        OutputStream outputStream = new ByteArrayOutputStream()
        service.files().get(file.id).executeMediaAndDownloadTo(outputStream)
        outputStream
    }

    protected toSave=false
    void save(){
        toSave=true
    }

    protected InputStream inputStream
    void save(InputStream inputStream) {
        toSave=true
        this.inputStream = inputStream
    }
    void save(java.io.File file) {
        toSave=true
        this.inputStream = file.newInputStream()
    }

    protected toRemove=false
    void removeFromDrive(){
        toRemove=true
    }

    protected String moveTo;
    void moveToFolder(String id){
        this.moveTo = id
    }

    void moveToFolder(File folder){
        assert folder.mimeType=='application/vnd.google-apps.folder'
        this.moveTo = folder.id
    }

    protected File execute(){
        if(toSave){
            assert toRemove==false,"Can't save and remove at same time"
            assert moveTo==null,"Can't save and move at same time"
            if( inputStream ){
                InputStreamContent inputStreamContent= new InputStreamContent(file.mimeType,inputStream)
                service.files().update(file.id,fileMetadata,inputStreamContent).execute()
            }else{
                service.files().update(file.id,fileMetadata).execute()
            }
        }
        if(toRemove) {
            assert toSave==false,"Can't save and remove at same time"
            assert moveTo==null,"Can't delete and move at same time"
            service.files().delete(file.id).execute()
        }
        if(moveTo){
            assert toSave==false,"Can't save and move at same time"
            assert moveTo==null,"Can't delete and move at same time"
            def update = service.files().update(file.id,null)
            update.addParents=moveTo
            update.removeParents=file.parents.join(',')
            update.execute()
        }
    }
}

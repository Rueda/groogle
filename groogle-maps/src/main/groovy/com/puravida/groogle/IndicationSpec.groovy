package com.puravida.groogle

import com.google.maps.model.LatLng
import groovy.transform.CompileStatic

@CompileStatic
class IndicationSpec implements IndicationTrait{

    protected LatLng latLng(){
        new LatLng(latitud,longitud)
    }

}

package groogle.rest

import com.google.api.services.drive.DriveScopes
import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.SheetScript

class ApplicationController {

    def index() {
        def sheets=[:]
        def sheetScript = SheetScript.instance

        sheetScript.listSpreadSheets.each {
            sheets["$it.id"]="$it.name"
        }
        println sheets
        respond sheets
     }
}

package com.puravida.groogle


import com.google.maps.model.DirectionsResult
import org.junit.Test

class PlacesMapsTest {

    @Test
    void directionTest() {
        InputStream resource = this.class.getResourceAsStream('/apikey.txt')
        String apikeyText = resource.text

        MapsScript.instance.with {
            //tag::login[]
            config {
                apiKey apikeyText
            }
            //end::login[]
            places {

            }
        }
    }

}

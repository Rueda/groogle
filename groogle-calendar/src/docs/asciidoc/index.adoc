= Groogle
Jorge Aguilera <jorge.aguilera@puravida-software.com>
:doctype: book
:toc: left
:toclevels: 4
:source-highlighter: coderay
:imagesdir: images
:icons: font
:setanchors:
:idprefix:
:idseparator: -
:toc-collapsable:
:multilanguage: es,gb
:multilanguage-toolbar: toc
:google-analytics-code: UA-687332-11
:theme: colony
:ensure-https:


Groovy + Google = Groogle

== Groogle Calendar

La libreria GroogleCalendar nos permite acceder y gestionar calendarios y eventos
de Google usando Groovy Script

== Dependencia

GroogleDrive depende de GroogleCore en la que delega la lógica de autentificación
y autorización

=== Gradle

[source,groovy,subs="attributes"]
----
repositories {
    jcenter()
}

compile 'com.puravida.groogle:groogle-core:{groogle-version}'
compile 'com.puravida.groogle:groogle-calendar:{groogle-version}'
----

=== Grape

[source,groovy,subs="attributes"]
----
@Grab(group = 'com.puravida.groogle', module = 'groogle-core', version = '{groogle-version}')
@Grab(group = 'com.puravida.groogle', module = 'groogle-calendar', version = '{groogle-version}')
----

== Identificación y permisos

.Groovy
[source,groovy]
----
include::{srcDir}/test/groovy/CalendarTest.groovy[tag=login]
----

.Java
[source,java]
----
include::{srcDir}/test/java/com/puravida/groogle/test/CalendarTest.java[tag=login]
----

== Listar calendarios

.Groovy
[source,groovy]
----
include::{srcDir}/test/groovy/CalendarTest.groovy[tag=list]
----

.Java
[source,java]
----
include::{srcDir}/test/java/com/puravida/groogle/test/CalendarTest.java[tag=list]
----

